package ru.bssg.yandexschool;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.speech.RecognizerIntent;
import android.support.v4.util.Pair;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import ru.bssg.yandexschool.data.TranslationResult;
import ru.bssg.yandexschool.db.DbUtility;
import ru.bssg.yandexschool.db.TranslationHelper;
import ru.bssg.yandexschool.network.NetworkUtility;
import ru.bssg.yandexschool.network.dictionary.Def;
import ru.bssg.yandexschool.network.dictionary.Dictionary;
import ru.bssg.yandexschool.network.dictionary.Mean;
import ru.bssg.yandexschool.network.dictionary.Syn;
import ru.bssg.yandexschool.network.dictionary.Tr;
import ru.yandex.speechkit.Error;
import ru.yandex.speechkit.SpeechKit;
import ru.yandex.speechkit.Synthesis;
import ru.yandex.speechkit.Vocalizer;
import ru.yandex.speechkit.VocalizerListener;

import static ru.bssg.yandexschool.Constants.KEY_PREFS_LANG_CODE_FROM_TAG;
import static ru.bssg.yandexschool.Constants.KEY_PREFS_LANG_CODE_TO_TAG;
import static ru.bssg.yandexschool.Constants.RESULT_SPEECH_REQUEST;

public class MainActivity extends AppCompatActivity
        implements
        DialogLanguageHost,
        VocalizerListener {

    // DONE Возможность добавить перевод в избранное
    // TODO Использовать настройки, сохраненные в Preferences
    // DONE Save/Restore TranslationResult
    // TODO DialogFragment - застилизовать - отступы, поля, размеры шрифта
    // DONE Стилизация темы - цвета, размер шрифта и т.п.
    // TODO Recycler массовое удаление
    // DONE Dictionary JSON -> TextView
    // DONE ContentProvider/Loader
    // DONE Вид для планшета?
    // TODO Проводить update/delete в таблицы SQLite в отдельных тредах?
    // TODO Settings активити? Какие настройки?
    // DONE SQLiteOpenHelper - поправить утечку
    // DONE RxJava zip переводчика и словаря
    // DONE RxJava отписываться после onPause, подписываться в onResume
    // DONE DialogFragment с Recycler или ListView для выбора языков
    // DONE DialogFragment - передавать входные переметры - язык, направление - язык "с какого" или "на какой"
    // DONE DialogFragment - возвращать параметры в активность - выбранный язык, направление - язык "с какого" или "на какой"
    // DONE RxJava combineLatest для поля ввода слова и выбора языков
    // DONE DB Сохранение json в базу данных
    // DONE DB Сохранение ответов веб-сервисов в базу
    // DONE DB Сохранять ответ dictionary в виде json
    // DON'T StorIO ? Нет - пока нет поддержки RxJava2
    // DONE RxJava Не посылать запрос, если в базе уже есть ответ
    // DONE Card View для термина с микрофоном, динамиком и иксом
    // DONE Text-to-speech yandex
    // DONE Поключение словаря, вывод результатов из словаря
    // DONE RxJava debounce для термина и выбора языков
    // DONE Табы и ViewPager для фрагментов "История" и "Избранное"
    // DONE Recycler для фрагментов "История" и "Избранное"
    // DONE база данных - хранить языки?
    // DONE Переделать диалог с ListView на Recycler?
    // DONE Диалог - элемент Recycler с "галочкой" для текущего языка?
    // DONE Recycler - стилизовать item-ы
    // DONE Recycler - изменение типа item-а при нажатии на значок "избранное"
    // DONE Recycler swipe-to-delete
    // DONE Text-to-speech yandex
    // DONE распознавание с микрофона
    // DONE Использовать Coordinator Layout для уменьшения AppBar?
    // TODO Loading индикатор ?
    // TODO показ ошибок ?
    // TODO сохранение Observable при изменении конфигурации - кэширование Observable?
    // DONE привязать Observable к жизненному циклу?
    // DONE Вынести ключи в gradle
    // DONE Вынести подстановку ключей в интерцепторы
    // DONE поправить стиль кнопок - сделать прозрачными но с Ripple-эфектом
    // DONE поправить стиль кнопок - сделать серенькими при setEnabled(false)
    // DONE сохранять в Preferences выбор языков, загружать их при старте приложения
    // TODO Bug Android - не работает регистронезависимый LIKE в SQLite - workaround - дополнительная колонка в таблице языков
    // TODO Bing переводчик?
    // TODO Google переводчик?
    // TODO Какие еще есть он-лайн переводчики? с API?
    // TODO база данных - хранить все возможные направления перевода?
    // TODO Скачивать массивы языков и направлений переводов синк-адаптером и периодически обновлять таблицы базы данных?
    // TODO база данных - ограничивать количество хранимых переводов? Сделать это настроечным параметром?
    // TODO Text-to-speech google?
    // TODO Разбить приложение на уровни ? MVP ?
    // TODO Dagger 2 ?
    // TODO ProGuard ?
    // TODO Тесты ?
    // TODO база данных - индексы?
    // TODO камера + OCR для распознавания текста с видео? Какие есть OCR? tesseract?
    // TODO "поделиться" переводом?

    private TranslationResult mTranslationResult = new TranslationResult(null, null, null, null, null);

    // SpeechKit
    private Vocalizer mVocalizer;

    // CardView
    private EditText mSentence;
    private ImageButton mClear;
    private ImageButton mAudio;
    private ImageButton mMicro;
    private ImageButton mFavorite;

    // Translation and Dictionary results
    private TextView mTranslation; // перевод aka tr.json
    private TextView mDictionary; // словарь aka dicservice.json

    // Toolbar
    private Button mLangFrom;
    private Button mLangTo;
    private ImageButton mSwap;

    // Константа, используемая для формирования спанов при показе термина из словаря
    public static final String n = "\n";

    // Локализация пользовательского интерфейса - например, ru, fr, en
    // Используется для запроса ресурсов из файлов в res/...
    String mLocale = "en"; // Default

    // База данных
    private TranslationHelper mHelper;

    // Интент для распознавания речи через google
    private static Intent recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

    // Результат запроса словаря сохраняется в базу данных в виде json-строки
    // Это исползуется для того, чтобы воссоздать иерархию классов по этой строке.
    private static Gson mGson = new GsonBuilder().setPrettyPrinting().create();

    // Реактивность - используется для подписок в onResume и отписок в onPause
    CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    // Используется для всего - комбинирования значений языков, введенного слова,
    // запросов веб-сервисов, кэширования в базу данных и т.п.
    Observable<TranslationResult> mSentenceObservable = null;

    // Мониторит текст кнопки на тулбаре
    Observable<String> mLangFromObservable = null;
    Observable<String> mLangToObservable = null;

    // Эмитит строку-направление перевода для веб-сервисов; например, ru-en или ru-fr
    Observable<String> mDirectionObservable = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState != null)
        {
            mTranslationResult = (TranslationResult) savedInstanceState.getSerializable(Constants.KEY_TRANSLATION_RESULT);
        }


        // Инициализация значений по-умолчанию из Preferences
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        mSentence = (EditText) findViewById(R.id.activity_main_sentence_edittext);
        mClear = (ImageButton) findViewById(R.id.activity_main_clear_imagebutton);
        mAudio = (ImageButton) findViewById(R.id.activity_main_audio_imagebutton);
        mMicro = (ImageButton) findViewById(R.id.activity_main_voice_imagebutton);
        mFavorite = (ImageButton) findViewById(R.id.activity_main_tranlate_favorite_imagebutton);
        mFavorite.setEnabled(false);

        FrameLayout historyPlaceHolder = (FrameLayout) findViewById(R.id.activity_main_history_fragment_placeholder);
        if(historyPlaceHolder != null)
        {
            getSupportFragmentManager().beginTransaction().add(
                    R.id.activity_main_history_fragment_placeholder,
                    HistoryFragment.newInstance(false))
                    .commit();
        }

        mFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isSelected = mFavorite.isSelected();
                mFavorite.setSelected(!isSelected);
                mTranslationResult.favorite = String.valueOf(mFavorite.isSelected());

                if(!TextUtils.isEmpty(mTranslationResult.word)
                        && !TextUtils.isEmpty(mTranslationResult.direction)
                        && !TextUtils.isEmpty(mTranslationResult.favorite)) {
                    DbUtility.saveTranslationResult(mTranslationResult, MainActivity.this.getApplicationContext());
                }
            }
        });

        mTranslation = (TextView) findViewById(R.id.activity_main_translate);
        mDictionary = (TextView) findViewById(R.id.activity_main_dictionary);

        mLangFrom = (Button) findViewById(R.id.activity_main_toolbar_lang_from_button);
        mLangTo = (Button) findViewById(R.id.activity_main_toolbar_lang_to_button);
        mSwap = (ImageButton) findViewById(R.id.activity_main_toolbar_swap_imagebutton);

        PackageManager packageManager = getPackageManager();
        if (recognizerIntent.resolveActivity(packageManager) != null) {
            // TODO переделать на RxJava?
            mMicro.setOnClickListener(view -> {

                String languageModel = mLangFrom.getTag().toString();
                // SEE http://stackoverflow.com/questions/10538791/how-to-set-the-language-in-speech-recognition-on-android
                recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, languageModel);
                recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, languageModel);
                recognizerIntent.putExtra("android.speech.extra.EXTRA_ADDITIONAL_LANGUAGES", new String[]{languageModel});
                try {
                    startActivityForResult(recognizerIntent, Constants.RESULT_SPEECH_REQUEST);
                } catch (ActivityNotFoundException a) {
                    // Такого не может произойти
                }
            });
        } else {
            mMicro.setEnabled(false);
        }

        mSwap.setOnClickListener(view -> {
            String language = mLangFrom.getText().toString();
            String tag = mLangFrom.getTag().toString();

            // Вначале нужно поменять значение тагов, так как
            // эмитится измененный текст кнопок
            // (на изменение тага подписаться нельзя)
            mLangFrom.setTag(mLangTo.getTag());
            mLangTo.setTag(tag);

            mLangFrom.setText(mLangTo.getText());
            mLangTo.setText(language);

            mTranslationResult.direction = getDirection();
        });

        mLocale = Utility.getLocale(this);

        // TODO возможный баг - при отсутствии значений в бандле нужно поискать их в преференсес и только потом подставлят дефолтные
        // TODO вынести дефолтные строки в строковые константы
        if (savedInstanceState != null) {
            mLangFrom.setTag(savedInstanceState.getString(KEY_PREFS_LANG_CODE_FROM_TAG, "ru"));
            mLangTo.setTag(savedInstanceState.getString(Constants.KEY_PREFS_LANG_CODE_TO_TAG, "en"));
        } else {
            mLangFrom.setTag(PreferenceManager.getDefaultSharedPreferences(this).getString(KEY_PREFS_LANG_CODE_FROM_TAG, "ru"));
            mLangTo.setTag(PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.KEY_PREFS_LANG_CODE_TO_TAG, "en"));
        }

        // По нажатию на кнопки выбора языков запускаем диалоги
        mLangFrom.setOnClickListener(view -> {
            Button button = (Button) view;
            Utility
                    //.getDialogLanguage(MainActivity.this, button.getText().toString(), button.getTag().toString(), Utility.LanguageDialogType.LANGUAGE_DIALOG_TYPE_FROM)
                    .getDialogLanguageRecycler(MainActivity.this, button.getText().toString(), button.getTag().toString(), Utility.LanguageDialogType.LANGUAGE_DIALOG_TYPE_FROM)
                    .show(getSupportFragmentManager(), "dialog_choose_language_from");
        });

        mLangTo.setOnClickListener(view -> {
            Button button = (Button) view;
            Utility
                    //.getDialogLanguage(MainActivity.this, button.getText().toString(), button.getTag().toString(), Utility.LanguageDialogType.LANGUAGE_DIALOG_TYPE_TO)
                    .getDialogLanguageRecycler(MainActivity.this, button.getText().toString(), button.getTag().toString(), Utility.LanguageDialogType.LANGUAGE_DIALOG_TYPE_TO)
                    .show(getSupportFragmentManager(), "dialog_choose_language_to");
        });

        mHelper = TranslationHelper.getInstance(this);

        SpeechKit.getInstance().configure(getApplicationContext(), BuildConfig.SPEECHKIT_API_KEY);

        mAudio.setOnClickListener(view -> {
            String textToSay = mSentence.getText().toString();
            String languageCode = mLangFrom.getTag().toString(); // Vocalizer.Language.RUSSIAN

            if (!TextUtils.isEmpty(textToSay)) {
                mVocalizer = Vocalizer.createVocalizer(languageCode, textToSay, true, Vocalizer.Voice.ALYSS);
                mVocalizer.setListener(MainActivity.this);
                mVocalizer.start();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(toolbar);
        {
            ActionBar bar = getSupportActionBar();
            if (bar != null)
                bar.setTitle("");
        }

        // Используются для получения локализованного
        // названия языка по коду языка из базы данных
        mLangFromObservable = Observable.just(mLangFrom.getTag().toString())
                .flatMap(code -> Utility.getLanguageDbObservable(mHelper, mLocale, code))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        ;


        mLangToObservable = Observable.just(mLangTo.getTag().toString())
                .flatMap(code -> Utility.getLanguageDbObservable(mHelper, mLocale, code))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        ;


        // Комбинируется со строкой слова для перевода
        // Нужен для того, чтобы перезапрашивать перевод при изменении
        // языков перевода
        mDirectionObservable =
                Observable.combineLatest(
                        RxTextView.textChanges(mLangFrom),
                        RxTextView.textChanges(mLangTo),
                        (one, two) -> getDirection()
                )
                        .startWith(getDirection())
                        .debounce(500, TimeUnit.MILLISECONDS)
        ;

        mSentenceObservable = Observable.combineLatest(
                RxTextView.textChanges(mSentence)
                        .startWith(mSentence.getText())
                        .debounce(500, TimeUnit.MILLISECONDS)
                        .map(CharSequence::toString)
                        .filter(text -> !TextUtils.isEmpty(text)),
                mDirectionObservable,
                Pair::new
        )
                //.switchMap(pair -> DbUtility.getTranslationFromDatabase(pair.first, pair.second, helper))
                .switchMap(pair -> DbUtility.getTranslationFromDatabase(pair.first, pair.second, MainActivity.this.getApplicationContext()))
                .switchMap(translationFromDb -> {
                    Log.d("happy", "translationFromDb:" + translationFromDb.word + ":" + translationFromDb.translation + ":");
                    if (translationFromDb.translation.equals("")) {
                        Log.d("happy", "translationFromDb.second.equals");

                        return Observable.zip(
                                NetworkUtility.getTranslateService().translateReactive(translationFromDb.direction, translationFromDb.word),
                                NetworkUtility.getDictionaryService().lookupReactive(translationFromDb.direction, translationFromDb.word),

                                ((translate, dictionaryResponse) -> {
                                    Log.d("happy", "Observable.zip translate: " + translate.getText().get(0));

                                    return new TranslationResult(
                                            translationFromDb.word, translationFromDb.direction, translate.getText().get(0), new Gson().toJson(dictionaryResponse.body()), "false"
                                    );
                                })

                        )
                                .doOnNext(translationResult -> Log.d("happy", "Observable.zip doOnNext" + translationResult.translation))
                                .onErrorResumeNext(Observable.empty())
                                .subscribeOn(Schedulers.io())
                                .switchMap(translationResult -> {
                                    if (!translationResult.translation.equals("")) {
                                        //return DbUtility.writeTranslationToDatabase(translationResult, helper);
                                        return DbUtility.writeTranslationToDatabase(translationResult, MainActivity.this.getApplicationContext());
                                    }
                                    else
                                        return Observable.empty();
                                })
                                .subscribeOn(Schedulers.computation())
                                ;

                    } else
                        return Observable.just(translationFromDb);
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.main_history: {
                Intent intent = new Intent(this, ListActivity.class);
                startActivity(intent);
                return true;
            }
            case R.id.main_settings: {
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_PREFS_LANG_CODE_FROM_TAG, mLangFrom.getTag().toString());
        outState.putString(Constants.KEY_PREFS_LANG_CODE_TO_TAG, mLangTo.getTag().toString());
        outState.putSerializable(Constants.KEY_TRANSLATION_RESULT, mTranslationResult);
    }

    private String getDirection() {
        return mLangFrom.getTag() + "-" + mLangTo.getTag();
    }


    @Override
    protected void onStart() {
        super.onStart();
        // DONE Подписаться RxJava
        if (mCompositeDisposable != null && !mCompositeDisposable.isDisposed()) {
            mCompositeDisposable.add(
                    mSentenceObservable
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    translationResult -> {
                                        //if (!TextUtils.isEmpty(translationResult.translation))
                                        if (translationResult.translation != null)
                                            mTranslation.setText(translationResult.translation);
                                        //if (!TextUtils.isEmpty(translationResult.dictionary)) {
                                        if (translationResult.dictionary != null) {
                                            try {
                                                Dictionary d = mGson.fromJson(translationResult.dictionary, Dictionary.class);
                                                SpannableStringBuilder builder = new SpannableStringBuilder("");
                                                if (d != null) {
                                                    Def def = d.getDef().get(0);
                                                    if (def != null) {
                                                        String translation = def.getText();

                                                        builder = append(builder, translation, new TextAppearanceSpan(this, R.style.translation_text), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                                                        builder.append(n);

                                                        String part = def.getPos();

                                                        builder = append(builder, part, new TextAppearanceSpan(this, R.style.translation_part), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                                                        builder.append(n);

                                                        List<Tr> trs = def.getTr();
                                                        if (trs != null) {
                                                            int trsSize = trs.size();
                                                            for (int i = 0; i < trsSize; i++) {
                                                                builder = append(builder, (i + 1) + " ", new TextAppearanceSpan(this, R.style.translation_number), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                                                                Tr tr = def.getTr().get(i);
                                                                builder = append(builder, tr.getText(), new TextAppearanceSpan(this, R.style.translation_tr), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


                                                                List<Syn> syns = tr.getSyn();
                                                                if (syns != null) {
                                                                    for (Syn s : syns) {
                                                                        builder = append(builder, ", " + s.getText(), new TextAppearanceSpan(this, R.style.translation_tr), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                                                    }
                                                                }
                                                                builder.append(n);
                                                                List<Mean> means = tr.getMean();
                                                                if (means != null) {
                                                                    int meansSize = means.size();
                                                                    if (meansSize > 0) {
                                                                        builder = append(builder, "   (", new TextAppearanceSpan(this, R.style.translation_mean), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                                                        for (int j = 0; j < meansSize; j++) {
                                                                            if (j > 0) {
                                                                                builder = append(builder, ", ", new TextAppearanceSpan(this, R.style.translation_mean), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                                                            }
                                                                            builder = append(builder, means.get(j).getText(), new TextAppearanceSpan(this, R.style.translation_mean), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                                                        }
                                                                        builder = append(builder, ")", new TextAppearanceSpan(this, R.style.translation_mean), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                                                        builder.append(n);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                mDictionary.setText(builder);
                                            } catch (Exception e) {
                                                // TODO обработать ошибки
                                            }
                                        }
                                        mFavorite.setTag(translationResult);
                                        mFavorite.setSelected(translationResult.favorite.equals("true"));
                                        mTranslationResult = translationResult;
                                    },
                                    exception -> Log.d("happy", exception.getMessage())
                            )
            );

            // TODO Переделать на коллбэк - нет никакой необходимости делать это реактивно
            mCompositeDisposable.add(
                    RxView.clicks(mClear)
                            .subscribe(
                                    click -> {
                                        mSentence.setText("");
                                    }
                            )
            );

            // TODO Переделать на коллбэк - нет никакой необходимости делать это реактивно
            mCompositeDisposable.add(
                    RxTextView.textChanges(mSentence)
                            .map(CharSequence::toString)
                            .subscribe(
                                    text -> {
                                        boolean notEmpty = !TextUtils.isEmpty(text);
                                        mClear.setEnabled(notEmpty);
                                        mAudio.setEnabled(notEmpty && isAudioEnabled(mLangFrom.getTag().toString()));
                                    }
                            )
            );

            mCompositeDisposable.add(
                    mLangFromObservable.subscribe(language -> {
                        mLangFrom.setText(language);
                        String languageCode = mLangFrom.getTag().toString();
                        mAudio.setEnabled(isAudioEnabled(languageCode) && !TextUtils.isEmpty(mSentence.getText().toString()));

                    })
            );

            mCompositeDisposable.add(
                    mLangToObservable.subscribe(language -> {
                        mLangTo.setText(language);
                    })
            );

            mCompositeDisposable.add(
                    RxTextView.textChanges(mTranslation)
                            .startWith(mTranslation.getText())
                            .subscribe(
                                    text -> {
                                        mFavorite.setEnabled(!TextUtils.isEmpty(text));
                                    }
                            )
            );

        }
    }

    private boolean isAudioEnabled(final String fromLanguageCode) {
        return fromLanguageCode.equals(Vocalizer.Language.RUSSIAN) ||
                fromLanguageCode.equals(Vocalizer.Language.ENGLISH) ||
                fromLanguageCode.equals(Vocalizer.Language.UKRAINIAN) ||
                fromLanguageCode.equals(Vocalizer.Language.TURKISH);
    }

    @Override
    protected void onStop() {
        // DONE Отписаться от подписок RxJava
        if (mCompositeDisposable != null && !mCompositeDisposable.isDisposed())
            mCompositeDisposable.dispose();
        if (isFinishing()) {
            // DONE Сохранить языки
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
            editor.putString(KEY_PREFS_LANG_CODE_FROM_TAG, mLangFrom.getTag().toString());
            editor.putString(KEY_PREFS_LANG_CODE_TO_TAG, mLangTo.getTag().toString());
            editor.apply();
        }
        super.onStop();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case RESULT_SPEECH_REQUEST: {
                if (resultCode == RESULT_OK && data != null && data.hasExtra(RecognizerIntent.EXTRA_RESULTS)) {
                    List<String> text = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    if (text != null) {
                        for (String t : text)
                            Log.d("happy", "onActivityResult, recognized sentence: " + t);
                        if (text.size() > 0)
                            mSentence.setText(text.get(0));
                    }
                }
                return;
            }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    // TODO Расширить SpannableStringBuilder и добавить туда этот метод
    public static SpannableStringBuilder append(SpannableStringBuilder builder, CharSequence text, Object what, int flags) {
        int start = builder.length();
        builder.append(text);
        builder.setSpan(what, start, builder.length(), flags);
        return builder;
    }

    // Вызывается при положительном закрытии диалога - передаются выбранный язык, код языка и
    // тип кнопки, значение языка в которой нужно поменять
    @Override
    public void newLanguage(String language, String code, Utility.LanguageDialogType type) {
        if (type == Utility.LanguageDialogType.LANGUAGE_DIALOG_TYPE_FROM) {
            // Вначале меняем таг, так как он эмитится только после изменения
            // текста кнопки (нет возможности подписаться на изменение тага)
            mLangFrom.setTag(code);
            mLangFrom.setText(language);
        } else if (type == Utility.LanguageDialogType.LANGUAGE_DIALOG_TYPE_TO) {
            mLangTo.setTag(code);
            mLangTo.setText(language);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        resetVocalizer();
    }

    private void resetVocalizer() {
        if (mVocalizer != null) {
            mVocalizer.cancel();
            mVocalizer = null;
        }
    }

    @Override
    public void onSynthesisBegin(Vocalizer vocalizer) {
        mAudio.setEnabled(false);
    }

    @Override
    public void onSynthesisDone(Vocalizer vocalizer, Synthesis synthesis) {
    }

    @Override
    public void onPlayingBegin(Vocalizer vocalizer) {
    }

    @Override
    public void onPlayingDone(Vocalizer vocalizer) {
        mAudio.setEnabled(true);
    }

    @Override
    public void onVocalizerError(Vocalizer vocalizer, Error error) {
        mAudio.setEnabled(true);
    }
}