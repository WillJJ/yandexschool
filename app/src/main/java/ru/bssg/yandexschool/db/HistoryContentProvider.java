package ru.bssg.yandexschool.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class HistoryContentProvider extends ContentProvider {

    private TranslationHelper mHelper;

    public static final String CONTENT_AUTHORITY = "ru.bssg.yandexschool.db";

    public static final Uri CONTENT_URI = Uri.parse(
            "content://" + CONTENT_AUTHORITY + "/history"
    );

    @Override
    public boolean onCreate() {
        mHelper = TranslationHelper.getInstance(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        SQLiteDatabase db = mHelper.getReadableDatabase();

        return db.query(
                HistoryTable.TABLE_HISTORY,
                new String[] {
                        "rowid as _id",
                        HistoryTable.COLUMN_WORD, HistoryTable.COLUMN_TRANSLATION, HistoryTable.COLUMN_DIRECTION, HistoryTable.COLUMN_DICTIONARY, HistoryTable.COLUMN_FAVORITE, HistoryTable.COLUMN_SOUND,
                },
                selection,
                selectionArgs,
                null,
                null,
                String.format("%s asc, %s asc", HistoryTable.COLUMN_WORD, HistoryTable.COLUMN_DIRECTION)
        );

//        return db.rawQuery(
//                String.format("select rowid as _id, %s, %s, %s, %s, %s, %s from %s;",
//                        HistoryTable.COLUMN_WORD, HistoryTable.COLUMN_TRANSLATION, HistoryTable.COLUMN_DIRECTION, HistoryTable.COLUMN_DICTIONARY, HistoryTable.COLUMN_FAVORITE, HistoryTable.COLUMN_SOUND,
//                        HistoryTable.TABLE_HISTORY),
//                null
//        );
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return "vnd.android.cursor.dir/vnd.translation.history";
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        SQLiteDatabase db = mHelper.getWritableDatabase();

        long id = db.insert(
                HistoryTable.TABLE_HISTORY,
                null,
                values
        );

        if (id > -1) {
            Uri inserted = ContentUris.withAppendedId(CONTENT_URI, id);
            getContext().getContentResolver().notifyChange(inserted, null);
            return inserted;
        } else
            return null;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = mHelper.getWritableDatabase();

        int deleteCount = db.delete(
                HistoryTable.TABLE_HISTORY,
                selection,
                selectionArgs
        );

        if (deleteCount > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return deleteCount;

    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = mHelper.getWritableDatabase();

        int updateCount = db.update(
                HistoryTable.TABLE_HISTORY,
                values,
                selection,
                selectionArgs
        );

        if (updateCount > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return updateCount;
    }
}
