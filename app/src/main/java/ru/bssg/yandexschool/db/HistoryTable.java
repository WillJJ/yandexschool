package ru.bssg.yandexschool.db;

import android.database.sqlite.SQLiteDatabase;

public class HistoryTable {

    public static final String TABLE_HISTORY = "history";

    public static final String COLUMN_WORD = "word";
    public static final String COLUMN_TRANSLATION = "translation";
    public static final String COLUMN_DIRECTION = "direction";
    public static final String COLUMN_DICTIONARY = "dictionary";
    public static final String COLUMN_FAVORITE = "favorite";
    public static final String COLUMN_SOUND = "sound";

    public static final String CREATE_TABLE = String.format(
            "create table %s (%s text not null,  %s text,  %s text not null,  %s text, %s boolean not null default 'false', %s text, PRIMARY KEY (%s, %s));",
                TABLE_HISTORY,
                COLUMN_WORD,  COLUMN_TRANSLATION, COLUMN_DIRECTION, COLUMN_DICTIONARY, COLUMN_FAVORITE, COLUMN_SOUND,
                COLUMN_WORD, COLUMN_DIRECTION
            );

    // TODO index
    public static final String CREATE_INDEX = "";

    public static void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
        populateTable(sqLiteDatabase);
    }

    public static void onUpgrade(SQLiteDatabase sqLiteDatabase, int newVersion, int oldVersion) {
    }


    public static void populateTable(SQLiteDatabase sqLiteDatabase)
    {
        String insert = "";

        insert = String.format("insert into %s (%s, %s, %s, %s) values ('%s', '%s', '%s', '%s');",
                TABLE_HISTORY,
                COLUMN_WORD, COLUMN_TRANSLATION,  COLUMN_DIRECTION, COLUMN_FAVORITE,
                "hi", "привет", "en-ru", "true"
                );
        sqLiteDatabase.execSQL(insert);


        insert = String.format("insert into %s (%s, %s, %s, %s) values ('%s', '%s', '%s', '%s');",
                TABLE_HISTORY,
                COLUMN_WORD, COLUMN_TRANSLATION,  COLUMN_DIRECTION, COLUMN_FAVORITE,
                "chevalier", "knight", "fr-en", "false"
        );
        sqLiteDatabase.execSQL(insert);

    }
}
