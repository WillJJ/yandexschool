package ru.bssg.yandexschool.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import ru.bssg.yandexschool.data.TranslationResult;

public class DbUtility {
    private DbUtility() {}

    public static Observable<TranslationResult> getTranslationFromDatabase(final String word, final String direction, final Context context) {

        return Observable.fromCallable(new Callable<TranslationResult>() {

            @Override
            public TranslationResult call() throws Exception {

                String translation = "";
                String dictionary = "";
                String favorite = "";

                Cursor cursor = context.getContentResolver().query(
                        HistoryContentProvider.CONTENT_URI,
                        new String[] {HistoryTable.COLUMN_TRANSLATION, HistoryTable.COLUMN_DICTIONARY},
                        HistoryTable.COLUMN_WORD + "=? and " + HistoryTable.COLUMN_DIRECTION + "=?",
                        new String[]{ word, direction },
                        null
                );

                if (cursor.moveToNext()) {
                    translation = cursor.getString(cursor.getColumnIndex(HistoryTable.COLUMN_TRANSLATION));
                    dictionary  = cursor.getString(cursor.getColumnIndex(HistoryTable.COLUMN_DICTIONARY));
                    favorite = cursor.getString(cursor.getColumnIndex(HistoryTable.COLUMN_FAVORITE));

                }
                if(!cursor.isClosed())
                    cursor.close();
                return new TranslationResult(word, direction, translation, dictionary,favorite);
            }
        });
    }

    /*
    public static Observable<TranslationResult> getTranslationFromDatabase(final String word, final String direction, final SQLiteOpenHelper helper) {

        return Observable.fromCallable(new Callable<TranslationResult>() {
            @Override
            public TranslationResult call() throws Exception {

                String translation = "";
                String dictionary = "";
                String favorite = "";

                Cursor cursor = helper.getReadableDatabase().query(
                        HistoryTable.TABLE_HISTORY,
                        new String[] {HistoryTable.COLUMN_TRANSLATION, HistoryTable.COLUMN_DICTIONARY},
                        HistoryTable.COLUMN_WORD + "=? and " + HistoryTable.COLUMN_DIRECTION + "=?",
                        new String[]{ word, direction },
                        null,
                        null,
                        null
                );
                if (cursor.moveToNext()) {
                    translation = cursor.getString(cursor.getColumnIndex(HistoryTable.COLUMN_TRANSLATION));
                    dictionary  = cursor.getString(cursor.getColumnIndex(HistoryTable.COLUMN_DICTIONARY));
                    favorite = cursor.getString(cursor.getColumnIndex(HistoryTable.COLUMN_FAVORITE));

                }
                cursor.close();
                return new TranslationResult(word, direction, translation, dictionary,favorite);
            }
        });
    }
    */

    public static Observable<TranslationResult> writeTranslationToDatabase(final String word, final String direction, final String translation, final String dictionary, final  String favorite, final Context context) {
        return Observable.fromCallable(new Callable<TranslationResult>() {

            @Override
            public TranslationResult call() throws Exception {
                ContentValues values = new ContentValues();


                Boolean isFavorite = Boolean.parseBoolean(favorite);

                values.put(HistoryTable.COLUMN_WORD, word);
                values.put(HistoryTable.COLUMN_DIRECTION, direction);
                values.put(HistoryTable.COLUMN_TRANSLATION, translation);
                values.put(HistoryTable.COLUMN_DICTIONARY, dictionary);
                values.put(HistoryTable.COLUMN_FAVORITE, isFavorite.toString());

                context.getContentResolver().insert(
                        HistoryContentProvider.CONTENT_URI,
                        values
                );

                return new TranslationResult(word, direction, translation, dictionary, favorite);

            }
        });
    }

    /*
    public static Observable<TranslationResult> writeTranslationToDatabase(final String word, final String direction, final String translation, final String dictionary, final  String favorite, final SQLiteOpenHelper helper) {
        return Observable.fromCallable(new Callable<TranslationResult>() {
            @Override
            public TranslationResult call() throws Exception {
                ContentValues values = new ContentValues();


                Boolean isFavorite = Boolean.parseBoolean(favorite);

                values.put(HistoryTable.COLUMN_WORD, word);
                values.put(HistoryTable.COLUMN_DIRECTION, direction);
                values.put(HistoryTable.COLUMN_TRANSLATION, translation);
                values.put(HistoryTable.COLUMN_DICTIONARY, dictionary);
                values.put(HistoryTable.COLUMN_FAVORITE, isFavorite.toString());


                long id = helper.getWritableDatabase().insert(
                        HistoryTable.TABLE_HISTORY,
                        null, // null column hack
                        values
                );
                Log.d("happy", "writeTranslationToDatabase id:" + id);
                return new TranslationResult(word, direction, translation, dictionary, favorite);
            }
        });
    }

    public static Observable<TranslationResult> writeTranslationToDatabase(final TranslationResult translationResult, final TranslationHelper helper)
    {
        return writeTranslationToDatabase(translationResult.word, translationResult.direction, translationResult.translation, translationResult.dictionary, translationResult.favorite, helper);
    }
    */

    public static Observable<TranslationResult> writeTranslationToDatabase(final TranslationResult translationResult, final Context context)
    {
        return writeTranslationToDatabase(translationResult.word, translationResult.direction, translationResult.translation, translationResult.dictionary, translationResult.favorite, context);
    }

    public static void saveTranslationResult(final TranslationResult result, final Context context) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                ContentValues values = new ContentValues();

                Boolean isFavorite = Boolean.parseBoolean(result.favorite);

                values.put(HistoryTable.COLUMN_FAVORITE, isFavorite.toString());

                context.getContentResolver().update(
                        HistoryContentProvider.CONTENT_URI,
                        values,
                        HistoryTable.COLUMN_WORD + "=? and " + HistoryTable.COLUMN_DIRECTION + "=?",
                        new String[]{ result.word, result.direction }
                );
            }
        }).start();
    }
}
