package ru.bssg.yandexschool.db;

import android.content.ContentProvider;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class TranslationHelper extends SQLiteOpenHelper {

    public static final int version = 1;
    public static final String dbFileName = "translation.db";

    private TranslationHelper(Context context) {
        super(context, dbFileName, null, version);
    }

    private static TranslationHelper helper;

    public static TranslationHelper getInstance(Context context)
    {
        if(helper == null)
            helper = new TranslationHelper(context);
        return helper;
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        HistoryTable.onCreate(sqLiteDatabase);
        LanguageTable.onCreate(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int newVersion, int oldVersion) {
        HistoryTable.onUpgrade(sqLiteDatabase, newVersion, oldVersion);
        LanguageTable.onUpgrade(sqLiteDatabase, newVersion, oldVersion);

    }
}
