package ru.bssg.yandexschool.network;


import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.bssg.yandexschool.BuildConfig;

public class NetworkUtility {

    // TODO make services available via singleton

    private NetworkUtility() {
    }

    public static YandexDictionaryService getDictionaryService() {
//        Retrofit mDictionaryRetrofit
//                = new Retrofit.Builder()
//                .client(new OkHttpClient.Builder()
//                        .addInterceptor(new DictionaryAPIKeyInterceptor())
//                        .build())
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .baseUrl(BuildConfig.DICTIONARY_API_ENDPOINT)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();


        return
                getRetrofitBuilder(BuildConfig.DICTIONARY_API_ENDPOINT, new DictionaryAPIKeyInterceptor())
                        .build()
                        .create(YandexDictionaryService.class);


    }

    private static Retrofit.Builder getRetrofitBuilder(String endPoint, Interceptor interceptor) {
        return new Retrofit.Builder()
                .client(new OkHttpClient.Builder()
                        .addInterceptor(interceptor)
                        .build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(endPoint)
                .addConverterFactory(GsonConverterFactory.create());
    }


    public static YandexTranslateService getTranslateService() {
//        Retrofit mTranslateRetrofit = new Retrofit.Builder()
//                .client(new OkHttpClient.Builder()
//                        .addInterceptor(new TranslateAPIInterceptor())
//                        .build())
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .baseUrl(BuildConfig.TRANSLATE_API_ENDPOINT)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//
//        return mTranslateRetrofit.create(YandexTranslateService.class);
        return
                getRetrofitBuilder(BuildConfig.TRANSLATE_API_ENDPOINT, new TranslateAPIInterceptor())
                        .build()
                        .create(YandexTranslateService.class);

    }
}
