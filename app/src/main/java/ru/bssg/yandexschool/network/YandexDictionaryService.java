package ru.bssg.yandexschool.network;




    //https://dictionary.yandex.net/api/v1/dicservice.json/lookup
        // ?key=dict.1.1.20170402T202525Z.1ed8dd7c23205718.efda3a2c4bdf834b16909622280b20b0d60840a1
        // &lang=fr-ru
        // &text=chevalier

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.bssg.yandexschool.network.dictionary.Dictionary;

public interface YandexDictionaryService {

    @GET("/api/v1/dicservice.json/lookup")
    public Call<Response<Dictionary>> lookup(
//            @Query("key") String key,
            @Query("lang") String lang,
            @Query("text") String text
    );

    @GET("/api/v1/dicservice.json/lookup")
    public Observable<Response<Dictionary>> lookupReactive(
//            @Query("key") String key,
            @Query("lang") String lang,
            @Query("text") String text
    );


}
