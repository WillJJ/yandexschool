package ru.bssg.yandexschool;

public class Constants {

    private Constants() {}

    public static final String KEY_DIALOG_LANGUAGE="KEY_DIALOG_LANGUAGE";
    public static final String KEY_DIALOG_CODE="KEY_DIALOG_CODE";
    public static final String KEY_DIALOG_DIRECTION="KEY_DIALOG_LOCALE";
    public static final int RESULT_SPEECH_REQUEST=555;

    public static final String KEY_TRANSLATION_RESULT = "KEY_TRANSLATION_RESULT";


    public static final String LANGUAGE_DIRECTION_FROM="KEY_LANGUAGE_DIRECTION_FROM";
    public static final String LANGUAGE_DIRECTION_TO="KEY_LANGUAGE_DIRECTION_TO";


    public static final String KEY_PREFS_LANG_FROM="KEY_PREFS_LANG_FROM";
    public static final String KEY_PREFS_LANG_CODE_FROM_TAG="KEY_PREFS_LANG_CODE_FROM_TAG";
    public static final String KEY_PREFS_LANG_TO="KEY_PREFS_LANG_FROM";
    public static final String KEY_PREFS_LANG_CODE_TO_TAG="KEY_PREFS_LANG_CODE_TO_TAG";
}
