package ru.bssg.yandexschool;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxbinding2.widget.RxAdapterView;
import com.jakewharton.rxbinding2.widget.RxTextView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.bssg.yandexschool.db.LanguageTable;
import ru.bssg.yandexschool.db.TranslationHelper;

public class DialogLanguage extends DialogFragment implements DialogInterface.OnClickListener {


    private SimpleCursorAdapter adapter;

    @Override
    public void onClick(DialogInterface dialog, int i) {
        try {
            ((DialogLanguageHost) getActivity()).newLanguage(mLanguage, mCode, mDirection);
        } catch (ClassCastException ex) {
        }
        cleanUp();
        dialog.dismiss();
    }

    private TranslationHelper mHelper;

    private String mLocale = "en";
    private String mLanguage = "";
    private String mCode = "";
    private Utility.LanguageDialogType mDirection;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Context context = getContext();

        mLocale = Utility.getLocale(context);

        // TODO Process arguments
        Bundle arguments = getArguments();
        if (arguments != null) {
            if (arguments.containsKey(Constants.KEY_DIALOG_LANGUAGE)) {
                mLanguage = arguments.getString(Constants.KEY_DIALOG_LANGUAGE);
                Log.d("happy", "onCreateDialog language: " + mLanguage);
            }
            if (arguments.containsKey(Constants.KEY_DIALOG_CODE)) {
                mCode = arguments.getString(Constants.KEY_DIALOG_CODE);
                Log.d("happy", "onCreateDialog locale: " + mCode);
            }
            if (arguments.containsKey(Constants.KEY_DIALOG_DIRECTION)) {
                mDirection = (Utility.LanguageDialogType) arguments.getSerializable(Constants.KEY_DIALOG_DIRECTION);
                Log.d("happy", "onCreateDialog direction: " + mDirection);
            }
        }

        mHelper = TranslationHelper.getInstance(context);

        View dialog = LayoutInflater.from(context).inflate(R.layout.dialog_language, null);
        TextView currentLanguage = (TextView) dialog.findViewById(R.id.dialog_current_language);
        EditText search = (EditText) dialog.findViewById(R.id.search_edittext);
        ImageButton clear = (ImageButton) dialog.findViewById(R.id.search_clear);
        ListView listView = (ListView) dialog.findViewById(R.id.dialog_language_listview);

        adapter = new SimpleCursorAdapter(
                context,
                android.R.layout.simple_list_item_1,
                null,
                new String[]{LanguageTable.COLUMN_LANGUAGE},
                new int[]{android.R.id.text1},
                0
        );

        listView.setAdapter(adapter);

        currentLanguage.setText(
                context.getString(R.string.language_dialog_message, mLanguage)
        );


        RxTextView.textChanges(search)
                .map(CharSequence::toString)
                .doOnNext(text -> {
                    clear.setEnabled(!TextUtils.isEmpty(text));
                })
                .subscribeOn(AndroidSchedulers.mainThread())
                .map(String::toLowerCase)
                .flatMap(text -> {
                    return Utility.getLanguagesDBObservable(mHelper, mLocale, text);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(adapter::swapCursor);

        RxAdapterView.itemClicks(listView)
                .subscribe(number -> {
                    Cursor cursor = adapter.getCursor();
                    cursor.moveToPosition(number);
                    mCode = cursor.getString(cursor.getColumnIndex(LanguageTable.COLUMN_CODE));
                    mLanguage = cursor.getString(cursor.getColumnIndex(LanguageTable.COLUMN_LANGUAGE));
                    currentLanguage.setText(
                            context.getString(R.string.language_dialog_message, mLanguage)
                    );
                });

        RxView.clicks(clear)
                .subscribe(some -> search.setText(""));


        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder
                .setView(dialog)
                .setCancelable(true)
                .setTitle(R.string.language_dialog_title)
                .setPositiveButton(R.string.dialog_button_ok, this)
                .setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        cleanUp();
                        dialog.cancel();
                    }
                });
        return builder.create();
    }

    public void cleanUp()
    {
        if(adapter != null)
        {
            Cursor cursor = adapter.getCursor();
            if(cursor != null)
                cursor.close();
            adapter.swapCursor(null);
        }
        if(mHelper != null)
            mHelper.close();

    }
}
