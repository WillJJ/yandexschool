package ru.bssg.yandexschool;

import android.content.Context;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.bssg.yandexschool.db.HistoryContentProvider;
import ru.bssg.yandexschool.db.HistoryTable;
import ru.bssg.yandexschool.db.TranslationHelper;
import ru.bssg.yandexschool.recycler.HistoryAdapter;


public class HistoryFragment extends Fragment {

    private RecyclerView mRecyclerView;

    private HistoryAdapter mAdapter;
    private boolean mShowFavorites = false;

    private static final String SHOW_FAVORITES = "SHOW_FAVORITES";
    private static int HISTORY_LOADER_ID = 555;

    public static HistoryFragment newInstance(boolean showFavorites)
    {
        HistoryFragment fragment = new HistoryFragment();

        Bundle bundle = new Bundle();
        bundle.putBoolean(SHOW_FAVORITES, showFavorites);
        fragment.setArguments(bundle);

        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle argsBundle = getArguments();
        if(argsBundle != null)
        {
            if(argsBundle.containsKey(SHOW_FAVORITES))
            {
                mShowFavorites = argsBundle.getBoolean(SHOW_FAVORITES, false);
            }
        }

        getLoaderManager().initLoader(HISTORY_LOADER_ID, Bundle.EMPTY, new LoaderManager.LoaderCallbacks<Cursor>() {

            @Override
            public Loader<Cursor> onCreateLoader(int id, Bundle args) {
                String selection = mShowFavorites ? HistoryTable.COLUMN_FAVORITE+"='true'" : "";
                String sortOrder = String.format("%s asc, %s asc", HistoryTable.COLUMN_WORD, HistoryTable.COLUMN_DIRECTION);
                return new CursorLoader(
                        getContext(),
                        HistoryContentProvider.CONTENT_URI,
                        null,
                        selection,
                        null,
                        sortOrder
                        );
            }

            @Override
            public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
                // Регистрируем курсор на получение обновлений в таблице
                cursor.setNotificationUri(
                        // через resolver
                        getContext().getContentResolver(),
                        // с нужным Uri
                        HistoryContentProvider.CONTENT_URI
                );
                // Заменяем курсор в адаптере на только что загруженный
                mAdapter.swapCursor(cursor);
            }

            @Override
            public void onLoaderReset(Loader<Cursor> loader) {
                mAdapter.swapCursor(null);
            }
        });

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycler, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_list_recycler);
        init(getContext(), mRecyclerView, mShowFavorites);
        return view;
    }

    private void init(final Context context, final RecyclerView rec, boolean showFavorites)
    {
        TranslationHelper translationHelper = TranslationHelper.getInstance(context);

        String selection = showFavorites ? HistoryTable.COLUMN_FAVORITE+"='true'" : "";

        mAdapter = new HistoryAdapter(null, getContext().getApplicationContext());

        rec.setAdapter(mAdapter);
        rec.setLayoutManager(new LinearLayoutManager(context));
        rec.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));


        ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                if(position != RecyclerView.NO_POSITION)
                {
                    mAdapter.remove(position);
                }
            }
        });
        helper.attachToRecyclerView(rec);
    }
}
