package ru.bssg.yandexschool;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {


    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        toolbar = (Toolbar) findViewById(R.id.activity_list_toolbar);
        tabLayout = (TabLayout) findViewById(R.id.activity_list_tabs);
        viewPager = (ViewPager) findViewById(R.id.activity_list_viewpager);

        // Устанавливаем Toolbar в качестве ActionBar
        // ActionBar удаляется с помощью темы в styles.xml
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // getSupportActionBar().setTitle("Tab activity");

        setupViewPager(viewPager);

        tabLayout.setupWithViewPager(viewPager);

        /*
        if(savedInstanceState == null)
        {
            HistoryFragment fragment = new HistoryFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.activity_list_placeholder, fragment).commit();
        }
        */
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(HistoryFragment.newInstance(false), getResources().getString(R.string.history));
        adapter.addFragment(HistoryFragment.newInstance(true), getResources().getString(R.string.favorites));

        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    static class ViewPagerAdapter extends FragmentPagerAdapter
    {
        // Контейнеры для фрагментов и заголовков табов
        private List<String> titles = new ArrayList<String>();
        private List<Fragment> fragments = new ArrayList<>();

        // Нужно обязательно вызвать конструктор суперкласса
        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles.get(position);
        }

        @Override
        public int getCount() {
            return titles.size();
        }

        // Добавление фрагментов во ViewPager
        public void addFragment(Fragment fragment, String title)
        {
            fragments.add(fragment);
            titles.add(title);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
