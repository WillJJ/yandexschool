package ru.bssg.yandexschool;


import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import io.reactivex.Observable;
import ru.bssg.yandexschool.db.LanguageTable;
import ru.bssg.yandexschool.db.TranslationHelper;

public class Utility {
    private Utility() {}

    public static DialogLanguageRecycler getDialogLanguageRecycler(
            @NonNull Context context, @NonNull String language, @NonNull String code, @NonNull LanguageDialogType type)
    {
        DialogLanguageRecycler dialog = new DialogLanguageRecycler();
        Resources resources = context.getResources();
        Bundle arguments = new Bundle();
        arguments.putString(Constants.KEY_DIALOG_LANGUAGE, language);
        arguments.putString(Constants.KEY_DIALOG_CODE, code);
        arguments.putSerializable(Constants.KEY_DIALOG_DIRECTION, type);
        dialog.setArguments(arguments);
        return dialog;
    }


    public static DialogLanguage getDialogLanguage(
            @NonNull Context context, @NonNull String language, @NonNull String code, @NonNull LanguageDialogType type)
    {
        DialogLanguage dialog = new DialogLanguage();
        Resources resources = context.getResources();
        Bundle arguments = new Bundle();
        arguments.putString(Constants.KEY_DIALOG_LANGUAGE, language);
        arguments.putString(Constants.KEY_DIALOG_CODE, code);
        arguments.putSerializable(Constants.KEY_DIALOG_DIRECTION, type);
        dialog.setArguments(arguments);
        return dialog;
    }

    public static Observable<String> getLanguageDbObservable(final SQLiteOpenHelper helper, final String locale, final String code)
    {
        return Observable.fromCallable(new Callable<String>() {
            @Override
            public String call() throws Exception {
                Cursor cursor = helper.getReadableDatabase().rawQuery(
                        "select CASE WHEN l2.language is null THEN l1.language ELSE l2.language END as language from language l1 left join language l2 on l1.code=l2.code and l2.locale=? where l1.locale='en' and l1.code=? order by language;",
                        new String[] {locale, code}
                );
                String result = "";
                if(cursor.moveToNext())
                    result = cursor.getString(cursor.getColumnIndex(LanguageTable.COLUMN_LANGUAGE));
                if(cursor.isAfterLast())
                    cursor.close();
                return result;
            }
        });
    }


    public static Observable<Cursor> getLanguagesDBObservable(final SQLiteOpenHelper helper, final String locale, final String expression)
    {
        return Observable.fromCallable(new Callable<Cursor>() {
            @Override
            public Cursor call() throws Exception {
                Cursor cursor = helper.getReadableDatabase().rawQuery(
                        //"select _id, code, language from (select l1.rowid as _id, l1.code, CASE WHEN l2.language is null THEN l1.language ELSE l2.language END as language from language l1 left join language l2 on l1.code=l2.code and l2.locale=?  where l1.locale='en') where language like ? order by language;",
                        "select _id, code, language from (select l1.rowid as _id, l1.code, CASE WHEN l2.language is null THEN l1.language ELSE l2.language END as language from language l1 left join language l2 on l1.code=l2.code and l2.locale=?  where l1.locale='en') where language like ? order by language;",
                        // "select l1.rowid as _id, l1.code, CASE WHEN l2.language is null THEN l1.language ELSE l2.language END as language from language l1 left join language l2 on l1.code=l2.code and l2.locale=?  where l1.locale='en' and language like ? order by language;",
                        new String[] {locale, "%%"+expression+"%%"}
                );
                return cursor;
            }
        });
    }



    public static Observable<List<Pair<String,String>>> getLanguagesDBListObservable(final SQLiteOpenHelper helper, final String locale, final String expression)
    {
        return getLanguagesDBObservable(helper, locale, expression)
                .map(cursor -> {
                    List<Pair<String,String>> result = new ArrayList<Pair<String, String>>();
                    while (cursor.moveToNext())
                    {
                        result.add(new Pair<String, String>(
                                cursor.getString(cursor.getColumnIndex(LanguageTable.COLUMN_CODE)),
                                cursor.getString(cursor.getColumnIndex(LanguageTable.COLUMN_LANGUAGE))
                        ));
                    }
                    cursor.close();
                    return result;
                });
    }

    public static String getLocale(@NonNull Context context)
    {
        String locale = "";
        Resources resources = context.getResources();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            locale = resources.getConfiguration().getLocales().get(0).getLanguage();
        else
            locale = resources.getConfiguration().locale.getLanguage();
        return locale;
    }
    public static enum LanguageDialogType
    {
        LANGUAGE_DIALOG_TYPE_FROM,
        LANGUAGE_DIALOG_TYPE_TO
    }
}
