package ru.bssg.yandexschool.recycler;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

import ru.bssg.yandexschool.R;
import ru.bssg.yandexschool.db.HistoryContentProvider;
import ru.bssg.yandexschool.db.HistoryTable;


public class HistoryAdapter extends RecyclerView.Adapter<HistoryViewHolder> {

    public static final int FAVORITE = 2;

    public static final int NORMAL = 1;

    private WeakReference<Context> context;

    private Cursor cursor;

    public HistoryAdapter(final Cursor cursor, Context context)
    {
        this.cursor = cursor;
        this.context = new WeakReference<Context>(context);
    }

    public void swapCursor(final Cursor cursor)
    {
        this.cursor = cursor;
        notifyDataSetChanged();
    }

    @Override
    public HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        // int layout = viewType == FAVORITE ? R.layout.list_item_favorite : R.layout.translation_list_item;

        // View view = inflater.inflate(layout, parent, false);
        View view = inflater.inflate(R.layout.translation_list_item, parent, false);

        if(viewType == FAVORITE)
        {
            ImageView image = (ImageView) view.findViewById(R.id.list_item_bookmark_imagebutton);
            image.setImageResource(R.drawable.ic_bookmark_yellow_24dp);
        }

        return new HistoryViewHolder(view, this);
    }

    @Override
    public int getItemViewType(int position) {
        if(cursor != null)
        {
            cursor.moveToPosition(position);
            String isFavorite = cursor.getString(cursor.getColumnIndex(HistoryTable.COLUMN_FAVORITE));

            if(isFavorite.equals("true"))
                return FAVORITE;
        }
        return NORMAL;
    }

    @Override
    public void onBindViewHolder(HistoryViewHolder holder, int position) {
        if(cursor != null)
        {
            cursor.moveToPosition(position);

            String word = cursor.getString(cursor.getColumnIndex(HistoryTable.COLUMN_WORD));
            String translation = cursor.getString(cursor.getColumnIndex(HistoryTable.COLUMN_TRANSLATION));
            String direction = cursor.getString(cursor.getColumnIndex(HistoryTable.COLUMN_DIRECTION));

            holder.lang.setText(direction);
            holder.word.setText(word);
            holder.translation.setText(translation);
        }
    }

    @Override
    public int getItemCount() {
        if(cursor != null)
            return cursor.getCount();
        return 0;
    }

    public void remove(int position) {


        if(context != null)
        {
            Context con = context.get();
            if(con != null)
            {
                cursor.moveToPosition(position);

                String id = cursor.getString(cursor.getColumnIndex("_id"));

                ContentResolver contentResolver = con.getContentResolver();

                int deleteCount = contentResolver.delete(
                        HistoryContentProvider.CONTENT_URI,
                        String.format("%s = ?", "rowid"),
                        new String[] {id}
                );
            }

        }
    }

    public void itemTypeChanged(int position) {
        if(context != null)
        {
            Context con = context.get();
            if(con != null)
            {
                cursor.moveToPosition(position);

                String id = cursor.getString(cursor.getColumnIndex("_id"));
                String favorite = cursor.getString(cursor.getColumnIndex(HistoryTable.COLUMN_FAVORITE));

                ContentResolver contentResolver = con.getContentResolver();

                Boolean isFavorite = !Boolean.parseBoolean(favorite);

                ContentValues values = new ContentValues();
                values.put(HistoryTable.COLUMN_FAVORITE, isFavorite.toString());

                int updateCount = contentResolver.update(
                        HistoryContentProvider.CONTENT_URI,
                        values,
                        String.format("%s = ?", "rowid"),
                        new String[] {id}
                );
            }
        }
    }
}
