package ru.bssg.yandexschool.recycler;

import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ru.bssg.yandexschool.R;

public class LanguagesAdapter extends RecyclerView.Adapter<LanguageViewHolder> {

    private OnItemClickListener listener;

    public void setCode(int position)
    {
        code = data.get(position).first;
        if(listener != null)
            listener.onItemClick(data.get(position));
    }

    public OnItemClickListener getListener()
    {
        return listener;
    }

    public interface OnItemClickListener {
        void onItemClick(final Pair<String, String> change);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public static final int LANGUAGE_SELECTED = 2;
    public static final int LANGUAGE_NORMAL = 1;

    private final List<Pair<String, String>> data;
    private String code;

    public LanguagesAdapter(final List<Pair<String, String>> data, final String selectedLanguageCode)
    {
        this.data = data;
        this.code = selectedLanguageCode;
    }

    @Override
    public int getItemViewType(int position) {
        return  data.get(position).first.equals(code) ? LANGUAGE_SELECTED : LANGUAGE_NORMAL;
    }

    @Override
    public LanguageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.language_list_item, parent, false);
        return new LanguageViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(LanguageViewHolder holder, int position) {
        int type = holder.getItemViewType();
        holder.language.setText(data.get(position).second);
        if(type == LANGUAGE_SELECTED)
            holder.mark.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
