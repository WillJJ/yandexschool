package ru.bssg.yandexschool.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import ru.bssg.yandexschool.R;

public class LanguageViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener  {

    private final LanguagesAdapter adapter;
    public TextView language;
    public ImageView mark;
    private int type;

    public LanguageViewHolder(View itemView, LanguagesAdapter adapter) {
        super(itemView);
        language = (TextView) itemView.findViewById(R.id.list_item_language_textview);
        mark = (ImageView) itemView.findViewById(R.id.list_item_chosen_imageview);
        this.adapter = adapter;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int position = getAdapterPosition();
        if (position != RecyclerView.NO_POSITION) {
            if(adapter != null) {
                adapter.setCode(position);
                // adapter.notifyItemChanged(position);
                adapter.notifyDataSetChanged();
            }
        }
    }
}
