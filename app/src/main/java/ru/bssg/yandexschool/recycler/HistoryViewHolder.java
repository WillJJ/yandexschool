package ru.bssg.yandexschool.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import ru.bssg.yandexschool.R;

public class HistoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final HistoryAdapter adapter;
    ImageButton bookmark;
    TextView word;
    TextView translation;
    TextView lang;

    public HistoryViewHolder(View view, final HistoryAdapter adapter) {
        super(view);
        this.adapter = adapter;
        bookmark = (ImageButton) view.findViewById(R.id.list_item_bookmark_imagebutton);
        word = (TextView) view.findViewById(R.id.list_item_term_textview);
        translation = (TextView) view.findViewById(R.id.list_item_translation_textview);
        lang = (TextView) view.findViewById(R.id.list_item_from_to_textview);

        bookmark.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(adapter != null) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                adapter.itemTypeChanged(position);
            }
        }
    }
}
