package ru.bssg.yandexschool;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxbinding2.widget.RxTextView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.bssg.yandexschool.db.TranslationHelper;
import ru.bssg.yandexschool.recycler.LanguagesAdapter;

public class DialogLanguageRecycler extends DialogFragment implements DialogInterface.OnClickListener, LanguagesAdapter.OnItemClickListener {


    private TranslationHelper mHelper;

    @Override
    public void onClick(DialogInterface dialog, int i) {
        try {
            ((DialogLanguageHost) getActivity()).newLanguage(mLanguage, mCode, mDirection);
        } catch (ClassCastException ex) {
            //
        }
        cleanUp();
    }


    private void processBundle(Bundle bundle)
    {
        if (bundle.containsKey(Constants.KEY_DIALOG_LANGUAGE)) {
            mLanguage = bundle.getString(Constants.KEY_DIALOG_LANGUAGE);
            Log.d("happy", "onCreateDialog language: " + mLanguage);
        }
        if (bundle.containsKey(Constants.KEY_DIALOG_CODE)) {
            mCode = bundle.getString(Constants.KEY_DIALOG_CODE);
            Log.d("happy", "onCreateDialog locale: " + mCode);
        }
        if (bundle.containsKey(Constants.KEY_DIALOG_DIRECTION)) {
            mDirection = (Utility.LanguageDialogType) bundle.getSerializable(Constants.KEY_DIALOG_DIRECTION);
            Log.d("happy", "onCreateDialog direction: " + mDirection);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(Constants.KEY_DIALOG_LANGUAGE, mLanguage);
        outState.putString(Constants.KEY_DIALOG_CODE, mCode);
        outState.putSerializable(Constants.KEY_DIALOG_DIRECTION, mDirection);

    }

    private String mLocale = "en";
    private String mLanguage = "";
    private String mCode = "";
    private Utility.LanguageDialogType mDirection;

    private  TextView currentLanguage;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Context context = getContext();

        mHelper = TranslationHelper.getInstance(context);

        mLocale = Utility.getLocale(context);

        // DONE Обрабатывать arguments
        // DONE Обрабатывать savedInstanceState
        if(savedInstanceState != null)
        {
            processBundle(savedInstanceState);
        }
        else {
            Bundle arguments = getArguments();
            if (arguments != null) {
                processBundle(arguments);
            }
        }

        View dialog = LayoutInflater.from(context).inflate(R.layout.dialog_language_recycler, null);
        currentLanguage = (TextView) dialog.findViewById(R.id.dialog_current_language);
        EditText search = (EditText) dialog.findViewById(R.id.search_edittext);
        ImageButton clear = (ImageButton) dialog.findViewById(R.id.search_clear);
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.dialog_language_recyclerview);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));


        updateLanguage();

        RxTextView.textChanges(search)
                .map(CharSequence::toString)
                .doOnNext(text -> clear.setEnabled(!TextUtils.isEmpty(text)))
                .subscribeOn(AndroidSchedulers.mainThread())
                .map(String::toLowerCase)
                .flatMap(text -> Utility.getLanguagesDBListObservable(mHelper, mLocale, text))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> {
                    LanguagesAdapter adapter = new LanguagesAdapter(list, mCode);
                    recyclerView.setAdapter(adapter);
                    adapter.setOnItemClickListener(this);
                });

//        RxAdapterView.itemClicks(listView)
//                .subscribe(number -> {
//                    Cursor cursor = adapter.getCursor();
//                    cursor.moveToPosition(number);
//                    mCode = cursor.getString(cursor.getColumnIndex(LanguageTable.COLUMN_CODE));
//                    mLanguage = cursor.getString(cursor.getColumnIndex(LanguageTable.COLUMN_LANGUAGE));
//                    currentLanguage.setText(
//                            context.getString(R.string.language_dialog_message, mLanguage)
//                    );
//                });

        RxView.clicks(clear)
                .subscribe(some -> search.setText(""));


        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder
                .setView(dialog)
                .setCancelable(true)
                .setTitle(R.string.language_dialog_title)
                .setPositiveButton(R.string.dialog_button_ok, this)
                .setNegativeButton(R.string.dialog_button_cancel, (d, id) -> {
                    cleanUp();
                    d.cancel();
                });
        return builder.create();
    }

    public void cleanUp()
    {
        if(mHelper != null)
            mHelper.close();
    }


    private void updateLanguage()
    {
        currentLanguage.setText(
                getContext().getString(R.string.language_dialog_message, mLanguage)
        );
        // Toast.makeText(getContext(), "Updated", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(Pair<String, String> change) {
        mCode = change.first;
        mLanguage = change.second;
        updateLanguage();
    }

}
