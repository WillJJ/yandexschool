package ru.bssg.yandexschool;

/**
 * Created by mk on 20/04/17.
 */

public interface DialogLanguageHost {
    void newLanguage(final String language, final String code, final Utility.LanguageDialogType type);
    // void negative();
}
