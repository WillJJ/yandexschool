package ru.bssg.yandexschool.data;


import java.io.Serializable;

public class TranslationResult implements Serializable {

    public TranslationResult(String word, String direction, String translation, String dictionary, String favorite) {
        this.word = word;
        this.direction = direction;
        this.translation = translation;
        this.dictionary = dictionary;
        this.favorite = favorite;
    }

    public String word;
    public String direction;
    public String translation;
    public String dictionary;
    public String favorite;
}
